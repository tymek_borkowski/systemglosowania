package wzim.sggw.votingsystem.utils;

import org.hibernate.validator.constraints.Range;
import wzim.sggw.votingsystem.model.VoteType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class CandidateRegisterDto {

    private int id;

    @NotNull
    private VoteType voteType;

    @Size(min = 2, max = 20)
    private String firstName;

    @Size(min = 2, max = 20)
    private String lastName;

    @Range(min = -2, max = Integer.MAX_VALUE)
    private int listNumber;

    @Range(min = -2, max = Integer.MAX_VALUE)
    private int positionNumber;

    public CandidateRegisterDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getListNumber() {
        return listNumber;
    }

    public void setListNumber(int listNumber) {
        this.listNumber = listNumber;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

}
