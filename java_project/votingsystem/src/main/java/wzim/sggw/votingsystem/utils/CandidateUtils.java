package wzim.sggw.votingsystem.utils;

import wzim.sggw.votingsystem.model.Candidate;
import wzim.sggw.votingsystem.model.VoteType;

public class CandidateUtils {

    public static int UNKNOWN_CANDIDATE_ID = -2;

    public static Candidate buildUnknownCandidate(VoteType voteType) {
        Candidate unknownCandidate = new Candidate();
        unknownCandidate.setFirstName("Głos nieważny.");
        unknownCandidate.setLastName("Głos nieważny.");
        unknownCandidate.setPositionNumber(UNKNOWN_CANDIDATE_ID);
        unknownCandidate.setListNumber(UNKNOWN_CANDIDATE_ID);
        unknownCandidate.setVoteType(voteType);

        return unknownCandidate;
    }

}
