package wzim.sggw.votingsystem.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wzim.sggw.votingsystem.model.Person;


@Controller
public class LoginController {

    private static final String ERROR_MESSAGE = "Wprowadziłeś nieprawidłowe dane. Spróbuj ponownie";

    @RequestMapping(value = "/")
    public String showLoginPage(Model model) {
        model.addAttribute("person", new Person());

        return "person/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showPersonErrorPage(@ModelAttribute(value = "person") Person person, Model model) {
        model.addAttribute("errorMessage", ERROR_MESSAGE);
        return "person/login";
    }

    @RequestMapping("/login/admin")
    public String showLoginAdminPage() {
        return "admin/login";
    }

    @RequestMapping(value = "/login/failure", method = RequestMethod.GET)
    public String showAdminErrorPage(@ModelAttribute(value = "admin") Person person, Model model) {
        model.addAttribute("errorMessage", ERROR_MESSAGE);
        return "admin/login";

    }
}
