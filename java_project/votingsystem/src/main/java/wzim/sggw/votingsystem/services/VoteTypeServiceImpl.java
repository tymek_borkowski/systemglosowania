package wzim.sggw.votingsystem.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import wzim.sggw.votingsystem.model.VoteType;
import wzim.sggw.votingsystem.repository.VoteTypeRepository;
import wzim.sggw.votingsystem.utils.VoteTypeRegisterDto;

import java.util.List;

@Service
public class VoteTypeServiceImpl implements VoteTypeService {

    private final VoteTypeRepository voteTypeRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public VoteTypeServiceImpl(VoteTypeRepository voteTypeRepository, ModelMapper modelMapper) {
        this.voteTypeRepository = voteTypeRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public VoteType findVoteTypeById(int id) {

        return voteTypeRepository.findVoteTypeById(id);
    }

    @Override
    public List<VoteType> findAll() {
        return voteTypeRepository.findAll();
    }

    @Override
    public void saveOrUpdate(VoteTypeRegisterDto voteType) {
        voteTypeRepository.save(convertToEntity(voteType));
    }

    @Override
    public void deleteById(int id) throws DataIntegrityViolationException {
        voteTypeRepository.deleteById(id);
    }

    private VoteType convertToEntity(VoteTypeRegisterDto voteTypeRegisterDto) {
        return modelMapper.map(voteTypeRegisterDto, VoteType.class);
    }
}
