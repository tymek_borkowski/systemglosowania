package wzim.sggw.votingsystem.config.spring.security;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static wzim.sggw.votingsystem.utils.VoterUtils.convertIdCardExpDateToDate;

public class PersonAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: "
                    + request.getMethod());
        }

        PersonAuthenticationToken authRequest = getAuthRequest(request);
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private PersonAuthenticationToken getAuthRequest(HttpServletRequest request) {
        String peselNumber = obtainPeselNumber(request).trim();
        String idCardNumber = obtainIdCardNumber(request).trim();
        String firstName = obtainFirstName(request).trim();
        String lastName = obtainLastName(request).trim();
        Date idCardExpDate = obtainIdCardExpDate(request);
        double lastIncomeValue = obtainLastIncomeValue(request);

        return new PersonAuthenticationToken(peselNumber, idCardNumber, firstName, lastName, idCardExpDate, lastIncomeValue);
    }

    private String obtainPeselNumber(HttpServletRequest request) {
        return request.getParameter("peselNumber");
    }

    private String obtainIdCardNumber(HttpServletRequest request) {
        return request.getParameter("idCardNumber");
    }

    private String obtainFirstName(HttpServletRequest request) {
        return request.getParameter("firstName");
    }

    private String obtainLastName(HttpServletRequest request) {
        return request.getParameter("lastName");
    }

    private Date obtainIdCardExpDate(HttpServletRequest request) {
        return convertIdCardExpDateToDate(request.getParameter("idCardExpDate"));
    }

    private double obtainLastIncomeValue(HttpServletRequest request) {
        return Double.valueOf(request.getParameter("lastIncomeValue"));
    }

}
