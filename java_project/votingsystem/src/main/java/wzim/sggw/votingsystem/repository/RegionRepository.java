package wzim.sggw.votingsystem.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import wzim.sggw.votingsystem.model.Region;

import java.util.List;

public interface RegionRepository extends CrudRepository<Region, Integer> {

    Region findRegionById(int id);

    List<Region> findAllByVoivodeship(String voivodeship);

    List<Region> findAllByCounty(String county);

    List<Region> findAllByCommune(String commune);

    List<Region> findAllByCommuneType(String communeType);

    List<Region> findAllByName(String name);

    List<Region> findAllByDescription(String description);

    List<Region> findAllByCity(String city);

    List<Region> findAllByNameIgnoreCaseContaining(String name);

    @Query("SELECT R FROM Region R WHERE UPPER(R.name) LIKE %:input% OR UPPER(R.city) LIKE %:input%")
    List<Region> findAllByInput(@Param("input") String input);

    List<Region> findAllByVoivodeshipAndCountyAndCommuneAndCommuneTypeAndNameAndDescriptionAndCity(String voivodeship, String county, String commune, String communeType, String name, String description, String city);

}
