package wzim.sggw.votingsystem.repository;

import org.springframework.data.repository.CrudRepository;
import wzim.sggw.votingsystem.model.Region;
import wzim.sggw.votingsystem.model.VoteType;

import java.util.List;

public interface VoteTypeRepository extends CrudRepository<VoteType, Integer> {

    VoteType findVoteTypeById(int id);

    VoteType findByRegion(Region region);

    List<VoteType> findAllByVoteName(String voteName);

    List<VoteType> findAllByRegion(Region region);

    List<VoteType> findAll();

}
