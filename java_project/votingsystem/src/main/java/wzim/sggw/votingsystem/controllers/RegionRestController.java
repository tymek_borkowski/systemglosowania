package wzim.sggw.votingsystem.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import wzim.sggw.votingsystem.model.Region;
import wzim.sggw.votingsystem.repository.RegionRepository;

import java.util.List;

@RestController
@RequestMapping(value = "/region")
public class RegionRestController {

    @Autowired
    private RegionRepository regionRepository;

    @GetMapping(value = "")
    public ResponseEntity<List<Region>> getRegionsByInput(@RequestParam("input") String input) {
        List<Region> regionList = regionRepository.findAllByInput(input);

        return new ResponseEntity<>(regionList, HttpStatus.OK);
    }

}
