package wzim.sggw.votingsystem.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RaportController {

    @RequestMapping("/raport")
    public String showRaportPage(){
        return "raport";
    }
}
