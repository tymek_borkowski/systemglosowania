package wzim.sggw.votingsystem.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import wzim.sggw.votingsystem.model.VoteType;
import wzim.sggw.votingsystem.services.CandidateService;
import wzim.sggw.votingsystem.services.VoteTypeService;
import wzim.sggw.votingsystem.utils.CandidateRegisterDto;
import wzim.sggw.votingsystem.utils.VoteTypeRegisterDto;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping(value = "/votetype")
public class VoteTypeController {

    private final VoteTypeService voteTypeService;
    private final CandidateService candidateService;

    @Autowired
    public VoteTypeController(VoteTypeService voteTypeService, CandidateService candidateService) {
        this.voteTypeService = voteTypeService;
        this.candidateService = candidateService;
    }


    @GetMapping(value = "")
    public String showAdminPanel() {
        return "admin/votetype";
    }


    @PostMapping(value = "/candidate/save")
    public String saveCandidate(@Valid @ModelAttribute("candidate") CandidateRegisterDto registerCandidate, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("alert", "Sprawdź dane głosowania.");
        } else {
            candidateService.saveOrUpdate(registerCandidate);
            model.addAttribute("success", "Nowy kandydat został dodany.");
        }

        return "admin/votetype";
    }

    @PostMapping(value = "/candidate/update")
    public String updateCandidate(@Valid @ModelAttribute("registerCandidate") CandidateRegisterDto registerCandidate, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("alert", "Sprawdź dane kandydata.");
        } else {
            candidateService.saveOrUpdate(registerCandidate);
            model.addAttribute("success", registerCandidate.getFirstName() + " " + registerCandidate.getLastName() + " kandydat został zmodyfikowany");
        }
        return "admin/votetype";
    }

    @PostMapping(value = "/candidate/delete/{candidateid}")
    public String deleteCandidate(@PathVariable("candidateid") int candidateId, Model model) {
        String message = "Kandydat został usunięty";
        String attribute = "success";
        try {
          candidateService.deleteById(candidateId);
        } catch (DataIntegrityViolationException ex) {
            message = "Nie można usunąć kandydata, na którego oddano głosy";
            attribute = "alert";
        } finally {
            model.addAttribute(attribute, message);
            return "admin/votetype";
        }
    }

    @PostMapping(value = "/update")
    public String updateVoteType(@Valid @ModelAttribute("registerVotetype") VoteTypeRegisterDto registerVotetype, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors() || (registerVotetype.getEndDate().compareTo(registerVotetype.getStartDate()) < 0)) {
            model.addAttribute("alert", "Sprawdź dane głosowania.");
        } else {
            voteTypeService.saveOrUpdate(registerVotetype);
            model.addAttribute("success", registerVotetype.getVoteName() + " głosowanie zostało zmodyfikowane");
        }
        return "admin/votetype";
    }


    @ModelAttribute
    public void addAttributes(Model model, HttpSession session) {
        model.addAttribute("registerCandidate", new CandidateRegisterDto());
        model.addAttribute("registerVoteType", new VoteTypeRegisterDto());
        VoteType selectedVoteType = (VoteType) session.getAttribute("selectedVoteType");
        model.addAttribute("selectedVoteType", selectedVoteType);
        model.addAttribute("candidates", candidateService.findAllByVoteType(selectedVoteType));
    }


}

