package wzim.sggw.votingsystem.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "region", schema = "dev")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwh_id")
    private int id;

    @Column(name = "voivodeship")
    private Integer voivodeship;

    @Column(name = "county")
    private Integer county;

    @Column(name = "commune")
    private Integer commune;

    @Column(name = "commune_type")
    private Integer communeType;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "city")
    private String city;

    @JsonIgnore
    @OneToMany(mappedBy = "region")
    private List<VoteType> voteTypes;
    @JsonIgnore
    @OneToMany(mappedBy = "region")
    private List<Person> persons;

    public Region() {

    }

    public Region(Integer voivodeship, Integer county, Integer commune, Integer communeType, String name, String description, String city) {
        this.voivodeship = voivodeship;
        this.county = county;
        this.commune = commune;
        this.communeType = communeType;
        this.name = name;
        this.description = description;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getVoivodeship() {
        return voivodeship;
    }

    public void setVoivodeship(Integer voivodeship) {
        this.voivodeship = voivodeship;
    }

    public Integer getCounty() {
        return county;
    }

    public void setCounty(Integer county) {
        this.county = county;
    }

    public Integer getCommune() {
        return commune;
    }

    public void setCommune(Integer commune) {
        this.commune = commune;
    }

    public Integer getCommuneType() {
        return communeType;
    }

    public void setCommuneType(Integer communeType) {
        this.communeType = communeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<VoteType> getVoteTypes() {
        return voteTypes;
    }

    public void setVoteTypes(List<VoteType> voteTypes) {
        this.voteTypes = voteTypes;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "Region{" +
                "id=" + id +
                ", voivodeship='" + voivodeship + '\'' +
                ", county=" + county +
                ", commune=" + commune +
                ", communeType=" + communeType +
                ", name=" + name +
                ", description='" + description + '\'' +
                ", city='" + city + '\'' +
                ", voteTypes=" + voteTypes +
                ", persons=" + persons +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Region region = (Region) o;
        return id == region.id &&
                county == region.county &&
                commune == region.commune &&
                communeType == region.communeType &&
                name == region.name &&
                Objects.equals(voivodeship, region.voivodeship) &&
                Objects.equals(description, region.description) &&
                Objects.equals(city, region.city) &&
                Objects.equals(voteTypes, region.voteTypes) &&
                Objects.equals(persons, region.persons);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, voivodeship, county, commune, communeType, name, description, city, voteTypes, persons);
    }
}
