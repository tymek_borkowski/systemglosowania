package wzim.sggw.votingsystem.config.spring.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;

public class PersonAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private String firstName;
    private String lastName;
    private Date idCardExpDate;
    private double lastIncomeValue;

    public PersonAuthenticationToken(Object principal, Object credentials, String firstName, String lastName, Date idCardExpDate, double lastIncomeValue) {
        super(principal, credentials);
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCardExpDate = idCardExpDate;
        this.lastIncomeValue = lastIncomeValue;
        super.setAuthenticated(false);
    }

    public PersonAuthenticationToken(Object principal, Object credentials, String firstName, String lastName, Date idCardExpDate, double lastIncomeValue,
                                     Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.firstName = firstName;
        this.lastName = lastName;
        this.idCardExpDate = idCardExpDate;
        this.lastIncomeValue = lastIncomeValue;
        super.setAuthenticated(true);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getIdCardExpDate() {
        return idCardExpDate;
    }

    public double getLastIncomeValue() {
        return lastIncomeValue;
    }
}
