package wzim.sggw.votingsystem.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Date;

public interface CustomUserDetailsService {

    UserDetails loadUserByPeselNumberAndFirstNameAndLastNameAndIdCardExpDateAndLastIncomeValue(String peselNumber,
                                                                                               String firstName,
                                                                                               String lastName,
                                                                                               Date idCardExpDate,
                                                                                               double lastIncomeValue)
            throws UsernameNotFoundException;
}
