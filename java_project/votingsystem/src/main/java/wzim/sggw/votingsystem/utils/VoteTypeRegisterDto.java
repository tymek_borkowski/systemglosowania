package wzim.sggw.votingsystem.utils;

import wzim.sggw.votingsystem.model.Region;

import javax.validation.constraints.Size;
import java.sql.Timestamp;


public class VoteTypeRegisterDto {

    private int id;

    @Size(min = 2, max = 20)
    private String voteName;

    private Region region = new Region();

    private Timestamp startDate;

    private Timestamp endDate;

    public VoteTypeRegisterDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVoteName() {
        return voteName;
    }

    public void setVoteName(String voteName) {
        this.voteName = voteName;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = Timestamp.valueOf(startDate);
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = Timestamp.valueOf(endDate);
    }
}
