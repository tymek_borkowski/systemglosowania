package wzim.sggw.votingsystem.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wzim.sggw.votingsystem.config.spring.security.PersonDetails;
import wzim.sggw.votingsystem.model.*;
import wzim.sggw.votingsystem.repository.CandidateRepository;
import wzim.sggw.votingsystem.repository.VoteRepository;
import wzim.sggw.votingsystem.repository.VoteTypeRepository;
import wzim.sggw.votingsystem.services.CandidateService;

import javax.validation.Valid;
import java.util.List;

import static java.lang.System.currentTimeMillis;
import static wzim.sggw.votingsystem.utils.CandidateUtils.UNKNOWN_CANDIDATE_ID;
import static wzim.sggw.votingsystem.utils.CandidateUtils.buildUnknownCandidate;

@Controller
public class ListController {

    private final VoteTypeRepository voteTypeRepository;
    private final CandidateService candidateService;
    private final CandidateRepository candidateRepository;
    private final VoteRepository voteRepository;

    @Autowired
    public ListController(VoteTypeRepository voteTypeRepository,
                          CandidateService candidateService,
                          CandidateRepository candidateRepository,
                          VoteRepository voteRepository) {
        this.voteTypeRepository = voteTypeRepository;
        this.candidateService = candidateService;
        this.candidateRepository = candidateRepository;
        this.voteRepository = voteRepository;
    }

    @RequestMapping("/person/list")
    public String showListPage(Model model) {
        Person loggedPerson = getLoggedPerson();

        int loggedPersonVotesNumber = loggedPerson.getVotes().size();
        if (loggedPersonVotesNumber > 0) {
            return "redirect:/vote/submitted";
        }

        VoteType sessionVoteType = getSessionVoteType(loggedPerson.getRegion());
        boolean voteTypeActive = isVoteTypeActive(sessionVoteType);
        boolean voteTypeExpired = isVoteTypeExpired(sessionVoteType);
        List<Candidate> localCandidates = getLocalCandidates(loggedPerson.getRegion());


        model.addAttribute("votes_size", loggedPersonVotesNumber);
        model.addAttribute("all_candidates_list", localCandidates);
        model.addAttribute("vote", new Vote());
        model.addAttribute("is_votetype_active", voteTypeActive);
        model.addAttribute("votetype_expired_flag", voteTypeExpired);

        return "person/list";
    }

    private boolean isVoteTypeExpired(VoteType voteType) {
        return voteType != null && currentTimeMillis() >= voteType.getEndDate().getTime();
    }

    private boolean isVoteTypeActive(VoteType voteType) {
        return (voteType != null)
                && (currentTimeMillis() >= voteType.getStartDate().getTime())
                && (currentTimeMillis() <= voteType.getEndDate().getTime());
    }

    @RequestMapping("/vote/submitted")
    public String showVoteSubmittedPage(Model model) {
        model.addAttribute("vote_submitted", "Głos został już oddany.");

        return "person/list";
    }

    @RequestMapping(value = "/vote/save", method = RequestMethod.POST)
    public String submitVote(@Valid @ModelAttribute("vote") Vote vote, BindingResult bindingResult) {
        Person loggedPerson = getLoggedPerson();

        if (bindingResult.hasErrors()) {
            return "redirect:/vote/save/error";
        }

        VoteType sessionVoteType = getSessionVoteType(loggedPerson.getRegion());

        vote.setVoteType(sessionVoteType);
        vote.setPerson(loggedPerson);


        Candidate selectedCandidate = candidateRepository.findCandidateById(vote.getCandidate().getId());
        if (selectedCandidate == null) {

            Candidate unknownCandidate = candidateRepository
                    .findCandidateByPositionNumberAndVoteType(UNKNOWN_CANDIDATE_ID, sessionVoteType);
            if (unknownCandidate == null) {
                unknownCandidate = buildUnknownCandidate(getSessionVoteType(loggedPerson.getRegion()));
                candidateRepository.save(unknownCandidate);
                unknownCandidate = candidateRepository
                        .findCandidateByPositionNumberAndVoteType(UNKNOWN_CANDIDATE_ID, sessionVoteType);
            }
            vote.setCandidate(unknownCandidate);
        } else {
            vote.setCandidate(selectedCandidate);
        }
        voteRepository.save(vote);

        return "redirect:/vote/submitted";
    }

    @RequestMapping(value = "/vote/save/error")
    public String showListPageWithSaveError(Model model) {
        model.addAttribute("save_message", "Niepoprawny głos");
        return "person/list";
    }

    private List<Candidate> getLocalCandidates(Region region) {
        return candidateRepository.findAllByVoteType(getSessionVoteType(region));
    }

    private VoteType getSessionVoteType(Region region) {
        return voteTypeRepository.findByRegion(region);
    }

    private Person getLoggedPerson() {
        PersonDetails personDetails = (PersonDetails) SecurityContextHolder.
                getContext()
                .getAuthentication()
                .getPrincipal();
        return personDetails.getPerson();
    }
}