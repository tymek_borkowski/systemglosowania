package wzim.sggw.votingsystem.model;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "candidate", schema = "dev")
public class Candidate {

    @Range(min = -2, max = Integer.MAX_VALUE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidate_gen")
    @SequenceGenerator(name = "candidate_gen", sequenceName = "dev.candidate_seq")
    @Column(name = "dwh_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "dwh_vote_type_id")
    private VoteType voteType;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "list_number")
    private int listNumber;

    @Column(name = "pos_number")
    private int positionNumber;

    @OneToMany(mappedBy = "candidate")
    private List<Vote> votes;

    public Candidate() {

    }

    public Candidate(VoteType voteType, String firstName, String lastName, int listNumber, int positionNumber) {
        this.voteType = voteType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.listNumber = listNumber;
        this.positionNumber = positionNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getListNumber() {
        return listNumber;
    }

    public void setListNumber(int listNumber) {
        this.listNumber = listNumber;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    @Override
    public String toString(){
        return this.getLastName().toUpperCase() + " " + this.getFirstName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candidate candidate = (Candidate) o;
        return id == candidate.id &&
                listNumber == candidate.listNumber &&
                positionNumber == candidate.positionNumber &&
                Objects.equals(voteType, candidate.voteType) &&
                Objects.equals(firstName, candidate.firstName) &&
                Objects.equals(lastName, candidate.lastName) &&
                Objects.equals(votes, candidate.votes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, voteType, firstName, lastName, listNumber, positionNumber, votes);
    }
}
