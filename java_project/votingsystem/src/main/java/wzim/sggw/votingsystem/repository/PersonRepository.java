package wzim.sggw.votingsystem.repository;

import org.springframework.data.repository.CrudRepository;
import wzim.sggw.votingsystem.model.Person;
import wzim.sggw.votingsystem.model.Region;

import java.util.Date;
import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    List<Person> findTop1ByFirstName(String firstName);

    Person findPersonById(int id);

    Person findByPeselNumberAndFirstNameAndLastNameAndIdCardExpDateAndLastIncomeValue(
            String peselNumber,
            String firstName,
            String lastName,
            Date idCardExpDate,
            double lastIncomeValue);

    Person findByIdCardExpDateAndPeselNumber(Date date, String peselNumber);

    List<Person> findAllByFirstName(String firstName);

    List<Person> findAllByLastName(String lastName);

    List<Person> findAllByRegion(Region region);

    Person findByPeselNumber(String peselNumber);
}
