package wzim.sggw.votingsystem.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import wzim.sggw.votingsystem.model.VoteType;
import wzim.sggw.votingsystem.services.VoteTypeService;
import wzim.sggw.votingsystem.utils.VoteTypeRegisterDto;


import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping(value = "/admin")
public class AdminPanelController {

    private final VoteTypeService voteTypeService;

    @Autowired
    public AdminPanelController(VoteTypeService voteTypeService) {
        this.voteTypeService = voteTypeService;
    }

    @GetMapping(value = "")
    public String showAdminPanel() {
        return "admin/panel";
    }

    @GetMapping(value = "/{voteTypeId}")
    public String showVoteTypePanel(@PathVariable("voteTypeId") int voteTypeId, HttpSession session) {
        VoteType selectedVoteType = voteTypeService.findVoteTypeById(voteTypeId);
        session.setAttribute("selectedVoteType", selectedVoteType);
        return "redirect:/votetype";
    }

    @PostMapping(value = "/save")
    public String saveVoteType(@Valid @ModelAttribute("voteType") VoteTypeRegisterDto voteType, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors() || (voteType.getEndDate().compareTo(voteType.getStartDate()) < 0)) {
            model.addAttribute("alert", "Sprawdź dane głosowania.");
        } else {

            voteTypeService.saveOrUpdate(voteType);
            model.addAttribute("success", "Nowe głosowanie zostało dodane.");
        }
        return "admin/panel";
    }

    @PostMapping(value = "/delete/{votetypeId}")
    public String deleteVoteType(@PathVariable("votetypeId") int votetypeId, Model model) {
        String message = "Głosowanie zostało usunięte";
        String attribute = "success";
        try {
            voteTypeService.deleteById(votetypeId);
        } catch (DataIntegrityViolationException ex) {
            message = "Nie można usunąć głosowania, w którym oddano głosy";
            attribute = "alert";
        } finally {
            model.addAttribute(attribute, message);
            return "admin/panel";
        }
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        List<VoteType> voteTypeList;
        voteTypeList = voteTypeService.findAll();
        model.addAttribute("voteTypeList", voteTypeList);
        model.addAttribute("registerVoteType", new VoteTypeRegisterDto());
    }
}
