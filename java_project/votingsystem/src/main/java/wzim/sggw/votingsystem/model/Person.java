package wzim.sggw.votingsystem.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "person", schema = "dev")
public class Person {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwh_id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "pesel_number")
    private String peselNumber;

    @Column(name = "id_card_number")
    private String idCardNumber;

    @Column(name = "id_card_exp_date")
    private Date idCardExpDate;

    @Column(name = "last_income_value", length = 8, precision = 2, columnDefinition = "NUMBER(8,2)")
    private double lastIncomeValue;

    @OneToMany(mappedBy = "person")
    private List<Vote> votes;

    @ManyToOne
    @JoinColumn(name = "dwh_region_id")
    private Region region;

    public Person() {

    }

    public Person(String firstName, String lastName, String peselNumber, String idCardNumber, Date idCardExpDate,
                  double lastIncomeValue) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.peselNumber = peselNumber;
        this.idCardNumber = idCardNumber;
        this.idCardExpDate = idCardExpDate;
        this.lastIncomeValue = lastIncomeValue;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPeselNumber() {
        return peselNumber;
    }

    public void setPeselNumber(String peselNumber) {
        this.peselNumber = peselNumber;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public Date getIdCardExpDate() {
        return idCardExpDate;
    }

    public void setIdCardExpDate(Date idCardExpDate) {
        this.idCardExpDate = idCardExpDate;
    }

    public double getLastIncomeValue() {
        return lastIncomeValue;
    }

    public void setLastIncomeValue(double lastIncomeValue) {
        this.lastIncomeValue = lastIncomeValue;
    }


    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }


    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getRole() {
        return "ROLE_PERSON";
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", peselNumber='" + peselNumber + '\'' +
                ", idCardNumber='" + idCardNumber + '\'' +
                ", idCardExpDate=" + idCardExpDate +
                ", lastIncomeValue=" + lastIncomeValue +
               // ", votes=" + votes + TODO ERROR
               // ", region=" + region + TODO ERROR
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                Double.compare(person.lastIncomeValue, lastIncomeValue) == 0 &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(peselNumber, person.peselNumber) &&
                Objects.equals(idCardNumber, person.idCardNumber) &&
                Objects.equals(idCardExpDate, person.idCardExpDate) &&
                Objects.equals(votes, person.votes) &&
                Objects.equals(region, person.region);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, peselNumber, idCardNumber, idCardExpDate, lastIncomeValue, votes, region);
    }
}
