package wzim.sggw.votingsystem.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import wzim.sggw.votingsystem.config.spring.security.PersonAuthenticationFilter;
import wzim.sggw.votingsystem.config.spring.security.PersonAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {


    @Configuration
    @Order(1)
    public static class App1ConfigurationAdapter extends WebSecurityConfigurerAdapter {
        public App1ConfigurationAdapter() {
            super();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/admin*")
                    .authorizeRequests()
                    .anyRequest()
                    .hasRole("ADMIN")
                    .antMatchers("/admin", "/admin/**").hasRole("ADMIN")
                    .and()
                    .formLogin()
                    .loginPage("/login/admin")
                    .loginProcessingUrl("/admin_login")
                    .failureUrl("/login/failure?error=loginError")
                    .defaultSuccessUrl("/admin")

                    .and()
                    .logout()
                    .logoutUrl("/admin_logout")
                    .logoutSuccessUrl("/login/admin")
                    .deleteCookies("JSESSIONID")

                    .and()
                    .exceptionHandling()
                    .accessDeniedPage("/403");
        }

        @Autowired
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication()
                    .withUser("admin1234")
                    .password("haslo1234")
                    .roles("ADMIN");
        }

    }


    @Configuration
    @Order(2)
    public static class App2ConfigurationAdapter extends WebSecurityConfigurerAdapter {

        private final PersonAuthenticationProvider personAuthenticationProvider;

        @Autowired
        public App2ConfigurationAdapter(PersonAuthenticationProvider personAuthenticationProvider) {
            this.personAuthenticationProvider = personAuthenticationProvider;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                    .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/person/list").hasRole("PERSON")
                    .antMatchers("/admin/panel").hasRole("ADMIN")
                    .and()
                    .formLogin()
                    .defaultSuccessUrl("/person/list")
                    .failureForwardUrl("/")
                    .loginPage("/").permitAll()

                    .and()
                    .logout()
                    .logoutUrl("/person_logout")
                    .logoutSuccessUrl("/")
                    .deleteCookies("JSESSIONID")

                    .permitAll();
        }

        @Override
        public void configure(WebSecurity web) {
            web.ignoring().antMatchers("/javax.faces.resource/**");
        }

        public PersonAuthenticationFilter authenticationFilter() throws Exception {
            PersonAuthenticationFilter filter = new PersonAuthenticationFilter();
            filter.setAuthenticationManager(authenticationManagerBean());
            filter.setAuthenticationFailureHandler(failureHandler());
            filter.setAuthenticationSuccessHandler(successHandler());
            return filter;
        }

        @Autowired
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(personAuthenticationProvider);
        }


        public SimpleUrlAuthenticationFailureHandler failureHandler() {
            return new SimpleUrlAuthenticationFailureHandler("/login?error=true");
        }

        public SimpleUrlAuthenticationSuccessHandler successHandler() {
            return new SimpleUrlAuthenticationSuccessHandler("/person/list");
        }


        @SuppressWarnings("deprecation")
        @Bean
        public static NoOpPasswordEncoder passwordEncoder() {
            return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
        }
    }


}