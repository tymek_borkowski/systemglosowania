package wzim.sggw.votingsystem.services;

import org.springframework.dao.DataIntegrityViolationException;
import wzim.sggw.votingsystem.model.Candidate;
import wzim.sggw.votingsystem.model.VoteType;
import wzim.sggw.votingsystem.utils.CandidateRegisterDto;

import java.util.List;

public interface CandidateService {

    List<Candidate> findAllByVoteType(VoteType voteType);

    void saveOrUpdate(CandidateRegisterDto candidate);

    void deleteById(int id) throws DataIntegrityViolationException;

}
