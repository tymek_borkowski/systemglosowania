package wzim.sggw.votingsystem.repository;

import org.springframework.data.repository.CrudRepository;
import wzim.sggw.votingsystem.model.Candidate;
import wzim.sggw.votingsystem.model.VoteType;

import java.util.List;

public interface CandidateRepository extends CrudRepository<Candidate, Integer> {

    Candidate findCandidateById(int id);

    Candidate findCandidateByPositionNumberAndVoteType(int positionNumber, VoteType voteType);

    List<Candidate> findAllByFirstName(String firstName);

    List<Candidate> findAllByLastName(String lastName);

    List<Candidate> findAllByVoteType(VoteType voteType);

    List<Candidate> findAllByVoteTypeAndListNumber(VoteType voteType, int listNumber);



}
