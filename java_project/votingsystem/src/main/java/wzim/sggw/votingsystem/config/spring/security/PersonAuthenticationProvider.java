package wzim.sggw.votingsystem.config.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import wzim.sggw.votingsystem.services.VoterDetailsService;

@Component
public class PersonAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private final VoterDetailsService voterDetailsService;

    @Autowired
    public PersonAuthenticationProvider(VoterDetailsService voterDetailsService) {
        this.voterDetailsService = voterDetailsService;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {

        if (authentication.getCredentials() == null) {
            logger.debug("Authentication failed: no credentials provided");
            throw new BadCredentialsException(
                    messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }

        String password = authentication.getCredentials()
                .toString();

        if (!password.equals(userDetails.getPassword())) {
            logger.debug("Authentication failed: password does not match stored value");
            throw new BadCredentialsException(
                    messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        PersonAuthenticationToken auth = (PersonAuthenticationToken) authentication;
        PersonDetails loadedPerson;

        loadedPerson = voterDetailsService
                .loadUserByPeselNumberAndFirstNameAndLastNameAndIdCardExpDateAndLastIncomeValue(
                        auth.getPrincipal().toString(), auth.getFirstName(), auth.getLastName(), auth.getIdCardExpDate(), auth.getLastIncomeValue());

        if (loadedPerson == null) {
            throw new InternalAuthenticationServiceException("PersonRepository returned null");
        }
        return loadedPerson;
    }
}