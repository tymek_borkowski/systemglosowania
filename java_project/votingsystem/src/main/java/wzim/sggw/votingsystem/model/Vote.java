package wzim.sggw.votingsystem.model;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.Objects;

@Entity
@Table(name = "vote", schema = "dev")
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vote_gen")
    @SequenceGenerator(name = "vote_gen", sequenceName = "dev.vote_seq")
    @Column(name = "dwh_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "dwh_person_id")
    private Person person;

    @Valid
    @ManyToOne
    @JoinColumn(name = "dwh_candidate_id")
    private Candidate candidate;

    @ManyToOne
    @JoinColumn(name = "dwh_vote_type_id")
    private VoteType voteType;

    public Vote() {

    }

    public Vote(Person person, Candidate candidate, VoteType voteType) {
        this.person = person;
        this.candidate = candidate;
        this.voteType = voteType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id=" + id +
                ", person=" + person +
                ", candidate=" + candidate +
                ", voteType=" + voteType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return id == vote.id &&
                Objects.equals(person, vote.person) &&
                Objects.equals(candidate, vote.candidate) &&
                Objects.equals(voteType, vote.voteType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, person, candidate, voteType);
    }
}
