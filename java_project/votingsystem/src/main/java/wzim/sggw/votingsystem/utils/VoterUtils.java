package wzim.sggw.votingsystem.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VoterUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoterUtils.class);
    private static final DateFormat ID_CARD_EXP_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);


    public static Date convertIdCardExpDateToDate(String idCardExpDate) {
        Date parsedDate;
        try {
            parsedDate = ID_CARD_EXP_DATE_FORMAT.parse(idCardExpDate);
        } catch (ParseException e) {
            LOGGER.warn("Wrong format of incoming date. Initializing random date.");
            e.printStackTrace();
            parsedDate = new Date();
        }
        return parsedDate;
    }

}
