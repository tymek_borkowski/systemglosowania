package wzim.sggw.votingsystem.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import wzim.sggw.votingsystem.model.Candidate;
import wzim.sggw.votingsystem.model.VoteType;
import wzim.sggw.votingsystem.repository.CandidateRepository;
import wzim.sggw.votingsystem.utils.CandidateRegisterDto;

import java.util.List;

@Service
public class CandidateServiceImpl implements CandidateService {

    private final CandidateRepository candidateRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CandidateServiceImpl(CandidateRepository candidateRepository, ModelMapper modelMapper) {
        this.candidateRepository = candidateRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<Candidate> findAllByVoteType(VoteType voteType) {
        return candidateRepository.findAllByVoteType(voteType);
    }

    @Override
    public void saveOrUpdate(CandidateRegisterDto candidate) {
        candidateRepository.save(convertToEntity(candidate));
    }

    @Override
    public void deleteById(int id) throws DataIntegrityViolationException {
        candidateRepository.deleteById(id);
    }

    private Candidate convertToEntity(CandidateRegisterDto candidateRegisterDto) {
        return modelMapper.map(candidateRegisterDto, Candidate.class);
    }

}

