package wzim.sggw.votingsystem.services;


import org.springframework.dao.DataIntegrityViolationException;
import wzim.sggw.votingsystem.model.VoteType;
import wzim.sggw.votingsystem.utils.VoteTypeRegisterDto;

import java.util.List;

public interface VoteTypeService {

    VoteType findVoteTypeById(int id);

    List<VoteType> findAll();

    void saveOrUpdate(VoteTypeRegisterDto voteType);

    void deleteById(int id) throws DataIntegrityViolationException;

}
