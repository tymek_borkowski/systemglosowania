package wzim.sggw.votingsystem.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import wzim.sggw.votingsystem.config.spring.security.PersonDetails;
import wzim.sggw.votingsystem.model.Person;
import wzim.sggw.votingsystem.repository.PersonRepository;

import java.util.Date;
import java.util.Optional;

@Service("voterDetailsService")
public class VoterDetailsService implements CustomUserDetailsService {

    private final PersonRepository personRepository;

    @Autowired
    public VoterDetailsService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public PersonDetails loadUserByPeselNumberAndFirstNameAndLastNameAndIdCardExpDateAndLastIncomeValue(
            String peselNumber, String firstName, String lastName, Date idCardExpDate, double lastIncomeValue)
            throws UsernameNotFoundException {

        Optional<Person> foundPerson = Optional.ofNullable(personRepository.findByPeselNumberAndFirstNameAndLastNameAndIdCardExpDateAndLastIncomeValue(
                peselNumber, firstName, lastName, idCardExpDate, lastIncomeValue));

        if (foundPerson.isPresent()) {
            return new PersonDetails(foundPerson.get());
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}