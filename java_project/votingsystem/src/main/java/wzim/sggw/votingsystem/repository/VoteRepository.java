package wzim.sggw.votingsystem.repository;

import org.springframework.data.repository.CrudRepository;
import wzim.sggw.votingsystem.model.Candidate;
import wzim.sggw.votingsystem.model.Person;
import wzim.sggw.votingsystem.model.Vote;
import wzim.sggw.votingsystem.model.VoteType;

import java.util.List;

public interface VoteRepository extends CrudRepository<Vote, Integer> {

    Vote findVoteById(int id);

    List<Vote> findAllByPerson(Person person);

    List<Vote> findAllByCandidate(Candidate candidate);

    List<Vote> findAllByVoteType(VoteType voteType);

    Vote save(Vote vote);

}
