package wzim.sggw.votingsystem;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.swing.text.DateFormatter;

@SpringBootApplication
public class VotingsystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(VotingsystemApplication.class, args);
    }
}
