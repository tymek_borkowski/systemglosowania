package wzim.sggw.votingsystem.model;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "vote_type", schema = "dev")
public class VoteType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vote_type_gen")
    @SequenceGenerator(name = "vote_type_gen", sequenceName = "dev.vote_type_seq")
    @Column(name = "dwh_id")
    private int id;

    @Column(name = "vote_name")
    private String voteName;

    @OneToMany(mappedBy = "voteType")
    private List<Vote> votes;

    @OneToMany(mappedBy = "voteType", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<Candidate> candidates;

    @ManyToOne
    @JoinColumn(name = "dwh_region_id")
    private Region region;

    @Column(name = "start_dt")
    private Timestamp startDate;

    @Column(name = "end_dt")
    private Timestamp endDate;

    public VoteType() {

    }

    public VoteType(String voteName, Region region) {
        this.voteName = voteName;
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVoteName() {
        return voteName;
    }

    public void setVoteName(String voteName) {
        this.voteName = voteName;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    /* @Override
    public String toString() {
        return "VoteType{" +
                "id=" + id +
                ", voteName='" + voteName + '\'' +
                ", votes=" + votes.size() +
                ", candidates=" + candidates +
                ", region=" + region +
                '}';
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteType voteType = (VoteType) o;
        return id == voteType.id &&
                Objects.equals(voteName, voteType.voteName) &&
                Objects.equals(votes, voteType.votes) &&
                Objects.equals(candidates, voteType.candidates) &&
                Objects.equals(region, voteType.region);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, voteName, votes, candidates, region);
    }
}
