var votingApp = angular.module("votingApp", []);

angular.module("votingApp").controller("regionCtrl", function($scope, $http) {
  $scope.selectedRegion;
  $scope.regions = [];

  var sticky = document.getElementById("sticky-bar");
  $scope.getRegions = function(input) {
    if (input.length > 2) {
      sticky.style.display = "block";
      input = input.toUpperCase();
      getRegionsData(input, function(data) {
        if (data.length > 0) {
          $scope.regions = data;
        }
      });
    } else {
      $scope.regions.length = 0;
      sticky.style.display = "none";
    }
  };

  function getRegionsData(input, callback) {
    $http.get("/region?input=" + input).then(function(response) {
      callback(response.data);
    });
  }

  $scope.setRegion = function(region) {
    $scope.selectedRegion = region;
    sticky.style.display = "none";
  };

 
});
