function toggleVisibilityCandidates() {
    var x = document.getElementById("candidates");
    if (x.style.display === "none") x.style.display = "block";
    else x.style.display = "none";
}

function toggleVisibilityVoteType() {
    var x = document.getElementById("votetype");
    if (x.style.display === "none") x.style.display = "block";
    else x.style.display = "none";
}

function toggleVisibilityResults() {
    var x = document.getElementById("results");
    if (x.style.display === "none") x.style.display = "block";
    else x.style.display = "none";
}

function toggleVisibilitySelectedVoteType() {
  var x = document.getElementById("selectedVoteType");
  if (x.style.display === "none") x.style.display = "block";
  else x.style.display = "none";
}

//login panel
window.onclick = function(event) {
		if (event.target == document.getElementById('loginwindow')) document.getElementById('loginwindow').style.display = "none";
}

function findCandidate() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("searchcandidate");
  filter = input.value.toUpperCase();
  table = document.getElementById("candidatetable");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

function findVoteType() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("searchvotetype");
  filter = input.value.toUpperCase();
  table = document.getElementById("votetypetable");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

//modal for candidate !!! DO EDYCJI (TEMPLATE)
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
